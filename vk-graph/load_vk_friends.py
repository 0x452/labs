# -*- coding: utf-8 -*-

import vk_api
import networkx as nx
import yaml

MAX_DEEP = 2
MAX_FRIENDS = 30
VK_LOGIN = '+79150611359'
VK_TOKEN = 'vk1.a.iNetI1cONwA9vlO-OM0HgGHHL7x3aM0VaxfnmooMgUoEjUoahts_0r6M-LlmAuGzC79t38v5FIKwc4PqS17YVUtlwbGuSw9uJYVgSc_DtkaPRI1lSQAFg6uavprnbvDRHk7bcJ4jtTHxQQLDh3SkrgGEjFjLZB4BJwcHJTi1wt8RZBOBSofFqpuZOfpXcjf9HcDusWYskGrrMM_6p8XdBA'
VK_IDS = [
    81354027, 126568936, 173792332, 507184348, 135986140,
    181948702, 553841503, 281482732, 53852554, 229302725
]

GRAPH_FILENAME = 'friends_graph.yaml'

def fill_graph(vk, graph, id, deep=1):
    max_cnt = MAX_FRIENDS
    if deep == 1:
        max_cnt = 500
    try:
        response = vk.friends.get(user_id=id, count=max_cnt)
    except vk_api.exceptions.ApiError as e:
        print(f"VK api error on id {id} and deep {deep}\n {e}")
        if(e.error['error_code']== 30): return
        else:
            print(f"VK api error on id {id} and deep {deep}\n {e}")
        return

    fr_in_lst = False
    for fr_id in response['items']:
        graph.add_edge(id, fr_id)
        if fr_id in VK_IDS:
            fr_in_lst = True
    if (not fr_in_lst) and (deep < MAX_DEEP):
        deep += 1
        for fr_id in response['items']:
            fill_graph(vk, graph, fr_id, deep)


def main():
    vk_session = vk_api.VkApi(login=VK_LOGIN, token=VK_TOKEN)
    vk = vk_session.get_api()
    friends_graph = nx.Graph()
    for id in VK_IDS:
        print(id)
        fill_graph(vk, friends_graph, id)

    print(f"Number of nodes: {friends_graph.number_of_nodes()}")
    with open(GRAPH_FILENAME, 'w') as fh:
        yaml.dump(friends_graph, fh)

if __name__ == "__main__":
    main()
